#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <ghost.h>
#include <ghost/instr.h>
#include <ghost/rand.h>
#include <essex-physics/matfuncs.h>
#include <essex-physics/cheb_toolbox.h>
#include "essexamples.h"

void * cheb_dos(void *varg)
{
    int m,i,rank;

    ghost_spmv_flags spmv_flags = GHOST_SPMV_DEFAULT;
    ghost_sparsemat_traits mtraits = GHOST_SPARSEMAT_TRAITS_INITIALIZER;
    ghost_sparsemat * mat;

    GHOST_INSTR_START("matrix_creation");
    essexamples_create_matrix(&mat,NULL,&mtraits);
    GHOST_INSTR_STOP("matrix_creation");

    ghost_rank(&rank,mat->context->mpicomm);

    char * dos_file = NULL;
    int R = 16, M = 1024;
    double eig_down = -10.;
    double eig_top  =  10.;
    double *mu;

    essexamples_get_output_file( &dos_file );
    essexamples_get_randvecnum( &R );
    essexamples_get_chebmoments( &M );
    essexamples_get_extremal_eig_range( &eig_down, &eig_top );

    mu = (double *)malloc(sizeof(double)*2*M);
    if (!mu) {
        fprintf(stderr,"malloc failed!\n");
        exit(EXIT_FAILURE);
    }
    
    for(m=0;m<2*M;m++) {
        mu[m]=0.;
    }

    double scale =  2./(eig_top - eig_down);
    double shift = 0.5*(eig_top + eig_down);

    ChebLoop_Options CL_Options;

    essexamples_get_cheb_mode(&CL_Options);
    essexamples_set_spmv_flags(&spmv_flags);

    if( mat->traits.datatype == (GHOST_DT_DOUBLE|GHOST_DT_COMPLEX ) ){
        cheb_dos_z( mat, scale, shift, M, R, mu, spmv_flags, CL_Options );
    }else{
        cheb_dos_d( mat, scale, shift, M, R, mu, spmv_flags, CL_Options );
    }

    if ( (rank == 0) && (dos_file != NULL)){
        double tmp = 1./R;
        for (m=0;m<2*M;m++) mu[m] *= tmp;
        cheb_toolbox_kpm_print_density(dos_file, 2*M, 1, mu , scale, shift, NULL, &cheb_toolbox_gibbs_Jackson );
    }

    essexamples_print_info(mat,0);

    ghost_sparsemat_destroy(mat);
    free(mu);
    return NULL;
}

int main(int argc, char* argv[])
{
    essexamples_process_options(argc,argv);

    ghost_init(argc,argv);
    ghost_rand_seed(GHOST_RAND_SEED_TIME,123);

    ghost_task *t;
    ghost_task_create(&t,GHOST_TASK_FILL_ALL, 0, &cheb_dos, NULL, GHOST_TASK_DEFAULT, NULL, 0);
    ghost_task_enqueue(t);
    ghost_task_wait(t);
    ghost_task_destroy(t);
    ghost_finalize();

    return EXIT_SUCCESS;
}


