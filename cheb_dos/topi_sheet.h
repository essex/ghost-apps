#ifndef TOPI_SHEET_H
#define TOPI_SHEET_H

#include <ghost.h>
#include <stdio.h>
#include <stdlib.h>


#ifdef __cplusplus
extern "C" {
#endif

int init_Topi_Sheet(int mpi_size_x, int mpi_size_y, int mpi_size_z );
int set_local_size_Topi_Sheet(ghost_idx_t l_x, ghost_idx_t l_y, ghost_idx_t l_z);
ghost_idx_t getDIM_Topi_Sheet();
int crsTopi_sheet( ghost_idx_t row, ghost_idx_t *nnz, ghost_idx_t *cols, void *vals);

#ifdef __cplusplus
}
#endif

	
#endif
