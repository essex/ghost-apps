#include "matfuncs.h"
#include <math.h>
#include <complex.h>
#include <ghost.h>



typedef struct {
	int mpi_rank_x;
	int mpi_rank_y;
	int mpi_rank_z;
	ghost_idx_t l_x;
	ghost_idx_t l_y;
	ghost_idx_t l_z;
	ghost_idx_t b;
	} grid_site_3D_b4;


typedef struct {
	int mpi_size_x;
	int mpi_size_y;
	int mpi_size_z;
	ghost_idx_t g_x;
	ghost_idx_t g_y;
	ghost_idx_t g_z;
	ghost_idx_t global_dim;
	ghost_idx_t l_x;
	ghost_idx_t l_y;
	ghost_idx_t l_z;
	ghost_idx_t local_dim;
	ghost_idx_t OBC_x;
	ghost_idx_t OBC_y;
	ghost_idx_t OBC_z;
	
	double * V;
	int32_t use_V;

	double m;
	double gp;
	double gm;
	double D1;
	double D2;
	double Bx;
	double By;
	double Bz;
	double Phi_y;
	double Phi_z;

	int32_t use_D;
	int32_t use_Z;
	int32_t model;

	double Dot_R;
	double Dot_V;

	} matfuncs_topi_sheet_t;


matfuncs_topi_sheet_t topi;

int init_Topi_Sheet(int mpi_size_x, int mpi_size_y, int mpi_size_z ){
	
	topi.OBC_x = 0;
	topi.OBC_y = 0;
	topi.OBC_z = 1;
	
	topi.l_x = 10;
	topi.l_y = 10;
	topi.l_z = 10;
	
	topi.local_dim = topi.l_z*topi.l_y*topi.l_x*4;
	
	topi.mpi_size_x = mpi_size_x;
	topi.mpi_size_y = mpi_size_y;
	topi.mpi_size_z = mpi_size_z;
	
	
	topi.global_dim = topi.mpi_size_z * topi.mpi_size_y * topi.mpi_size_x * topi.local_dim;
	topi.g_x = topi.mpi_size_x * topi.l_x;
	topi.g_y = topi.mpi_size_y * topi.l_y;
	topi.g_z = topi.mpi_size_z * topi.l_z;
	
	topi.V = NULL;
	topi.use_V  = 0;
	
	topi.m = 2.;
	
	topi.gp = 0.;
	topi.gm = 0.;
	topi.D1 = 0.;
	topi.D2 = 0.;
	topi.Bx = 0.;
	topi.By = 0.;
	topi.Bz = 0.;
	topi.Phi_y = 0.;
	topi.Phi_z = 0.;

	topi.use_D;
	topi.use_Z;
	topi.model;

	topi.Dot_R = 1.;
	topi.Dot_V = 0.;
return 0;
}

int set_local_size_Topi_Sheet(ghost_idx_t l_x, ghost_idx_t l_y, ghost_idx_t l_z){

	topi.l_x = l_x;
	topi.l_y = l_y;
	topi.l_z = l_z;
	topi.local_dim = topi.l_z*topi.l_y*topi.l_x*4;
	
	topi.global_dim = topi.mpi_size_z * topi.mpi_size_y * topi.mpi_size_x * topi.local_dim;
	topi.g_x = topi.mpi_size_x * topi.l_x;
	topi.g_y = topi.mpi_size_y * topi.l_y;
	topi.g_z = topi.mpi_size_z * topi.l_z;
	
return 0;
}

ghost_idx_t getDIM_Topi_Sheet(){
	return topi.g_x*topi.g_y*topi.g_z*4;
}

grid_site_3D_b4 index_2_grid3D_b4( ghost_idx_t idx ){
	grid_site_3D_b4 site;
	
	
	int rank = (int)(idx/topi.local_dim);
	ghost_idx_t l_idx = idx - rank*topi.local_dim;
	//printf("%d, %d, %d\n",rank, topi.local_dim, idx);
	
	site.mpi_rank_x = rank/(topi.mpi_size_y*topi.mpi_size_z);                   rank -= topi.mpi_size_y*topi.mpi_size_z*site.mpi_rank_x;
	site.mpi_rank_y = rank/(                topi.mpi_size_z); site.mpi_rank_z = rank -                  topi.mpi_size_z*site.mpi_rank_y;
	
	
	site.l_x = l_idx/(topi.l_y*topi.l_z*4);           l_idx -= topi.l_y*topi.l_z*4 * site.l_x;
	site.l_y = l_idx/(         topi.l_z*4);           l_idx -=          topi.l_z*4 * site.l_y;
	site.l_z = l_idx/                   4 ;  site.b = l_idx -                    4 * site.l_z;
	
	return site;
	}


ghost_idx_t grid3D_b4_2_index( grid_site_3D_b4  site ){
	
	if ( site.l_x >= topi.l_x ) { site.l_x -= topi.l_x; site.mpi_rank_x += 1; }
	if ( site.l_x <  0        ) { site.l_x += topi.l_x; site.mpi_rank_x -= 1; }
	if ( site.l_y >= topi.l_y ) { site.l_y -= topi.l_y; site.mpi_rank_y += 1; }
	if ( site.l_y <  0        ) { site.l_y += topi.l_y; site.mpi_rank_y -= 1; }
	if ( site.l_z >= topi.l_z ) { site.l_z -= topi.l_z; site.mpi_rank_z += 1; }
	if ( site.l_z <  0        ) { site.l_z += topi.l_z; site.mpi_rank_z -= 1; }
	
	if( site.mpi_rank_x == topi.mpi_size_x )  site.mpi_rank_x = 0;
	if( site.mpi_rank_x == -1              )  site.mpi_rank_x = topi.mpi_size_x-1;
	if( site.mpi_rank_y == topi.mpi_size_y )  site.mpi_rank_y = 0;
	if( site.mpi_rank_y == -1              )  site.mpi_rank_y = topi.mpi_size_y-1;
	if( site.mpi_rank_z == topi.mpi_size_z )  site.mpi_rank_z = 0;
	if( site.mpi_rank_z == -1              )  site.mpi_rank_z = topi.mpi_size_z-1;
	
	return ((((((     site.mpi_rank_x)*topi.mpi_size_y 
	                 + site.mpi_rank_y)*topi.mpi_size_z
	                 + site.mpi_rank_z)*topi.l_x
	                 + site.l_x       )*topi.l_y 
	                 + site.l_y       )*topi.l_z
	                 + site.l_z       )*4
	                 + site.b;
	}


/*
// default init

double * Topi_V = NULL;
int32_t Topi_use_V = 0;

ghost_idx_t L = 128;
ghost_idx_t W = 128;
ghost_idx_t H = 8;
ghost_idx_t P = 4;

//#define       P  4


int32_t PBC_L = 1;
int32_t PBC_W = 1;
int32_t PBC_H = 0;

double m     = 2.;
double gp    = 0.;
double gm    = 0.;
double D1    = 0.;
double D2    = 0.;
double Bx    = 0.0;
double By    = 0.0;
double Bz    = 0.;
double Phi_y = 0.;
double Phi_z = 0.;

int32_t use_D   = 0;
int32_t use_Z   = 0;
int32_t model   = 0;


double Dot_R = 25.;
double Dot_V = 0.52;
//double Dot_V = 0.3;
//double Dot_V = 0.;

*/

/*
int crsTopi_V_init_anderson_disorder( int seed, double gamma){ //, COMM_t * comm ){
	
	srand(seed);
	ghost_idx_t i,N = L*W*H;
	Topi_V = malloc(sizeof(double)*N);
	Topi_use_V = 1;
	
	for(i=0;i<N;i++) Topi_V[i] = gamma*( ((double)rand())/((double)RAND_MAX) - 0.5);
	
	//MPI_Bcast(Topi_V, N, 0, *comm );
	return 0;
	}

ghost_datatype crsTopi_get_DT(){
	return (GHOST_DT_DOUBLE|GHOST_DT_COMPLEX);
}

double crsTopi_get_eig_range_low(){
	double val = -5.2;
	if ( use_D ) val -= fabs(D1) + fabs(D2);
	if ( use_Z ) val -= (fabs(gp)+fabs(gm))*(fabs(Bx)+fabs(By)+fabs(Bz));
	return val -  fabs(Dot_V);
}

double crsTopi_get_eig_range_high(){
	double val = 5.5;
	if ( use_D ) val += fabs(D1) + fabs(D2);
	if ( use_Z ) val += (fabs(gp)+fabs(gm))*(fabs(Bx)+fabs(By)+fabs(Bz));
	return  val + fabs(Dot_V);
}

ghost_idx_t crsTopi_get_dim(){
	return  L*W*H*P;
}

ghost_idx_t crsTopi_get_max_nnz(){
	return  P*7;
}

int crsTopi_set_size( ghost_idx_t l, ghost_idx_t w, ghost_idx_t h ){
	L=l;
	W=w;
	H=h;
	return  0;
}

int crsTopi_get_size( ghost_idx_t *l, ghost_idx_t *w, ghost_idx_t *h ){
	*l=L;
	*w=W;
	*h=H;
	return  0;
}
*/


int crsTopi_sheet( ghost_idx_t row, ghost_idx_t *nnz, ghost_idx_t *cols, void *vals){

	matfuncs_topi_sheet_t * topi_s;
	topi_s = &topi;

double * Topi_V = topi_s->V;
int32_t Topi_use_V = topi_s->use_V;

ghost_idx_t L = topi_s->g_x;
ghost_idx_t W = topi_s->g_y;
ghost_idx_t H = topi_s->g_z;
ghost_idx_t P = 4;
//#define       P  4


ghost_idx_t OBC_L = topi_s->OBC_x;
ghost_idx_t OBC_W = topi_s->OBC_y;
ghost_idx_t OBC_H = topi_s->OBC_z;

double m     = topi_s->m;
double gp    = topi_s->gp;
double gm    = topi_s->gm;
double D1    = topi_s->D1;
double D2    = topi_s->D2;
double Bx    = topi_s->Bx;
double By    = topi_s->By;
double Bz    = topi_s->Bz;
double Phi_y = topi_s->Phi_y;
double Phi_z = topi_s->Phi_z;

int32_t use_D   = topi_s->use_D;
int32_t use_Z   = topi_s->use_Z;
int32_t model   = topi_s->model;


double Dot_R = topi_s->m;
double Dot_V = topi_s->m;

	ghost_idx_t N = L*W*H*P;

	if ((row >-1 ) && (row <N)){       //  defined output -- write entries of #row in *cols and *vals
	                                   //                    return number of entries
		double complex * zvals = vals;
		ghost_idx_t i = 0 ;
		
		grid_site_3D_b4 site = index_2_grid3D_b4( row );
		grid_site_3D_b4 site_tmp;
		
		ghost_idx_t l = site.l_x + site.mpi_rank_x*topi_s->l_x;
		ghost_idx_t w = site.l_y + site.mpi_rank_y*topi_s->l_y;
		ghost_idx_t h = site.l_z + site.mpi_rank_z*topi_s->l_z;
		ghost_idx_t p = site.b;
		
		
		//if((p >=4) || (p<0)) { printf("error crsTopi\n"); fflush(stdout); exit(0); }
		
		double V = 0.;
		/*
		if(Topi_use_V) V += Topi_V[(l*W+w)*H+h];
		double Dot_D = 4*Dot_R;
		double Dot_cx= 2*Dot_R;
		double Dot_cy= 2*Dot_R;
		double Dot_x = fmod( l , Dot_D );
		double Dot_y = fmod( w , Dot_D );
		
		if( (((Dot_cx-Dot_x)*(Dot_cx-Dot_x) + (Dot_cy-Dot_y)*(Dot_cy-Dot_y)) < Dot_R*Dot_R )
		     //&&  (    (h==0) || (h==H-1) 
		     //      || (h==1) || (h==H-2)
		     //    ) 
		      )  V += Dot_V;
		*/

		double complex  phase_x = cexp(2.*M_PI*I*(Phi_y*h + Phi_z*w));
		double complex cphase_x = conj(phase_x);
		
		//if( !model ){
		
		ghost_idx_t   iHx[4][2]   = {{ 0 ,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHx_l[4][2] = {{-0.5,        0.5},
		                              {    0.5,  0.5},
		                              {   -0.5, -0.5},
		                              {-0.5,        0.5}};
		double complex vHx_u[4][2] = {{-0.5,       -0.5},
		                              {    0.5, -0.5},
		                              {    0.5, -0.5},
		                              { 0.5,        0.5}};

		ghost_idx_t   iHy[4][2]   = {{ 0L,        3 },
		                              {     1 , 2 },
		                              {     1 , 2 },
		                              { 0 ,        3 }};
		double complex vHy_l[4][2] = {{  -0.5,            -I*0.5},
		                              {         0.5,-I*0.5},
		                              {      -I*0.5,  -0.5},
		                              {-I*0.5,              0.5}};
		double complex vHy_u[4][2] = {{  -0.5,             I*0.5},
		                              {         0.5, I*0.5},
		                              {       I*0.5,  -0.5},
		                              { I*0.5,              0.5}};

		ghost_idx_t   iHz[4][2]   = {{ 0 , 1        },
		                              { 0 , 1        },
		                              {        2 , 3 },
		                              {        2 , 3 }};
		double complex vHz_l[4][2] = {{  -0.5,  0.5           },
		                              {  -0.5,  0.5           },
		                              {              -0.5, 0.5},
		                              {              -0.5, 0.5}};
		double complex vHz_u[4][2] = {{  -0.5, -0.5           },
		                              {   0.5,  0.5           },
		                              {             -0.5, -0.5},
		                              {              0.5,  0.5}};

		

		

		if(l>=OBC_L)   {  zvals[i] = vHx_l[p][0];
		                     site_tmp = site; site_tmp.l_x--; site_tmp.b=iHx[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHx_l[p][1];            
		                     site_tmp = site; site_tmp.l_x--; site_tmp.b=iHx[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(w>=OBC_W)   {  zvals[i] = vHy_l[p][0];
		                     site_tmp = site; site_tmp.l_y--; site_tmp.b=iHy[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHy_l[p][1];            
		                     site_tmp = site; site_tmp.l_y--; site_tmp.b=iHy[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(h>=OBC_H)   {  zvals[i] = vHz_l[p][0];            
		                     site_tmp = site; site_tmp.l_z--; site_tmp.b=iHz[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHz_l[p][1];            
		                     site_tmp = site; site_tmp.l_z--; site_tmp.b=iHz[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		switch(p) {
		case 0 :             zvals[i] = V + m; if(use_Z) zvals[i] +=  Bz*(gp+gm); cols[i] = row;     i++;
		         if(use_D) { zvals[i] =  D1-I*D2;                                  cols[i] = row+1 ;  i++;}
		         if(use_Z) { zvals[i] = (Bx-I*By)*(gp+gm);                         cols[i] = row+2 ;  i++;}
		     break;
		case 1 : if(use_D) { zvals[i] =  D1+I*D2;                                 cols[i] = row-1 ;  i++;}
		                     zvals[i] = V - m; if(use_Z) zvals[i] +=  Bz*(gp-gm);  cols[i] = row;     i++;
		         if(use_Z) { zvals[i] = (Bx-I*By)*(gp-gm);                         cols[i] = row+2 ;  i++;}
		     break;
		case 2 : if(use_Z){ zvals[i] = (Bx+I*By)*(gp+gm);                         cols[i] = row-2 ;  i++;}
		                     zvals[i] =  V + m;  if(use_Z) zvals[i] -= Bz*(gp+gm); cols[i] = row;     i++;
		         if(use_D) { zvals[i] = -D1+I*D2;                                  cols[i] = row+1 ;  i++;}
		     break;
		case 3 : if(use_Z) { zvals[i] = (Bx+I*By)*(gp-gm);                        cols[i] = row-2 ;  i++;}
		         if(use_D) { zvals[i] = -D1-I*D2;                                  cols[i] = row-1 ;  i++;}
		                     zvals[i] = V - m; if(use_Z) zvals[i] -=  Bz*(gp-gm);  cols[i] = row;     i++;
		     break;
		}
		
		if(h<H-OBC_H ) {    zvals[i] = vHz_u[p][0];            
		                     site_tmp = site; site_tmp.l_z++; site_tmp.b=iHz[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHz_u[p][1];            
		                     site_tmp = site; site_tmp.l_z++; site_tmp.b=iHz[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(w<W-OBC_W) {    zvals[i] = vHy_u[p][0];            
		                     site_tmp = site; site_tmp.l_y++; site_tmp.b=iHy[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHy_u[p][1];            
		                     site_tmp = site; site_tmp.l_y++; site_tmp.b=iHy[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }
		if(l<L-OBC_L) {    zvals[i] = vHx_u[p][0];            
		                     site_tmp = site; site_tmp.l_x++; site_tmp.b=iHx[p][0]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;
		                zvals[i] = vHx_u[p][1];            
		                     site_tmp = site; site_tmp.l_x++; site_tmp.b=iHx[p][1]; cols[i]= grid3D_b4_2_index(site_tmp);
		                     i++;  }

			
			
		//}
		/*
		else{
		
		ghost_idx_t  iHx[4][2] = {{ 0 ,        3 },
		                                {     1 , 2    },
		                                {     1 , 2    },
		                                { 0 ,        3 }};
		double complex vHx_l[4][2] = {{-0.5,                   I},
		                              {      -0.5,       I       },
		                              {           I, 0.5         },
		                              {     I,              0.5  }};
		double complex vHx_u[4][2] = {{-0.5,              -    I},
		                              {      -0.5,  -    I       },
		                              {      -    I, 0.5         },
		                              {-    I,              0.5  }};

		ghost_idx_t   iHy[4][2]   = {{ 0,         3 },
		                                   {     1 , 2    },
		                                   {     1 , 2    },
		                                   { 0 ,        3 }};
		double complex vHy_l[4][2] = {{  -0.5,             1.0 },
		                              {        -0.5,  -1.0      },
		                              {         1.0,   0.5      },
		                              {  -1.0,              0.5 }};
		double complex vHy_u[4][2] = {{  -0.5,              -1.0},
		                              {        -0.5,   1.0       },
		                              {        -1.0,   0.5       },
		                              {   1.0,              0.5  }};

		ghost_idx_t   iHz[4][2] = {{ 0,      2    },
		                                 {     1 ,    3 },
		                                 { 0 ,     2    },
		                                 {     1 ,    3 }};
		double complex vHz_l[4][2] = {{  -0.5,         I       },
		                              {        -0.5,       -I   },
		                              {    I       ,   0.5      },
		                              {         -I,         0.5 }};
		double complex vHz_u[4][2] = {{  -0.5,         -I       },
		                              {        -0.5,         I   },
		                              {    -I,         0.5       },
		                              {          I,         0.5  }};

		if(l>0   ) {  zvals[i] = vHx_l[p][0]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][0];  i++;
		              zvals[i] = vHx_l[p][1]*cphase_x;   cols[i] = P*(H*(W*(l-1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		if(w>0   ) {  zvals[i] = vHy_l[p][0];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][0];  i++;
		              zvals[i] = vHy_l[p][1];            cols[i] = P*(H*(W* l     + w-1  ) + h    ) + iHy[p][1];  i++;  }
		if(h>0   ) {  zvals[i] = vHz_l[p][0];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][0];  i++;
		              zvals[i] = vHz_l[p][1];            cols[i] = P*(H*(W* l     + w    ) + h-1  ) + iHz[p][1];  i++;  }
		switch(p) {
		case 0 :  
		                     zvals[i] = V + 2.;  cols[i] = row;     i++;
		     break;
		case 1 : 
		                     zvals[i] = V + 2.;  cols[i] = row;     i++;
		     break;
		case 2 : 
		                     zvals[i] = V - 2.;  cols[i] = row;     i++;
		     break;
		case 3 : 
		                     zvals[i] = V - 2.;  cols[i] = row;     i++;
		     break;
		}
		if(h<H-1 ) {  zvals[i] = vHz_u[p][0];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][0];  i++;
		              zvals[i] = vHz_u[p][1];            cols[i] = P*(H*(W* l     + w    ) + h+1  ) + iHz[p][1];  i++;  }
		if(w<W-1 ) {  zvals[i] = vHy_u[p][0];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][0];  i++;
		              zvals[i] = vHy_u[p][1];            cols[i] = P*(H*(W* l     + w+1  ) + h    ) + iHy[p][1];  i++;  }
		if(l<L-1 ) {  zvals[i] = vHx_u[p][0]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][0];  i++;
		              zvals[i] = vHx_u[p][1]*phase_x;    cols[i] = P*(H*(W*(l+1 ) + w    ) + h    ) + iHx[p][1];  i++;  }
		
		
		}
		*/




	*nnz = i;
	return  0;
	}

	*nnz = -1;
	return  1;              //  error
}

/*

int vecTopi_Observable_z(void *Observable, ghost_idx_t row, ghost_idx_t n_cols, void *vec_vals){
	
	ghost_idx_t N = L*W*H*P;

	ghost_idx_t l =  row                  /(W*H*P);
	ghost_idx_t w = (row-  l*W   *H   *P )/(  H*P);
	ghost_idx_t h = (row- (l*W+w)*H   *P )/(    P);
	ghost_idx_t p =  row-((l*W+w)*H+h)*P;

		//ghost_idx_t p =  row%P;
		//ghost_idx_t h =  ((row-p)/P)%H;
		//ghost_idx_t w =  ((row-p-P*h)/(P*H))%W;
		//ghost_idx_t l =  ((row-p-P*h-P*H*w)/(P*H*W))%L;

	
	
	double complex * vals = vec_vals;
	double * sum = Observable;
	
	ghost_idx_t i;
	for(i=0;i<n_cols;i++) sum[h] += creal(vals[i])*creal(vals[i]) + cimag(vals[i])*cimag(vals[i]);
	
	return 0;
}


int vecTopi_Observable_z_reduce(void *Observable){
#ifdef GHOST_HAVE_MPI
        {
            ghost_mpi_op op;
            ghost_mpi_datatype dt;
            ghost_mpi_op_sum(&op,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            ghost_mpi_datatype_get(&dt,GHOST_DT_DOUBLE|GHOST_DT_REAL);
            MPI_Allreduce( MPI_IN_PLACE, Observable, H, dt, op, MPI_COMM_WORLD);
        }
#endif
return 0;
}



void vecTopi_surface_state( ghost_idx_t row, ghost_idx_t col,  void * val){
	
	ghost_idx_t N = L*W*H*P;


	//	ghost_idx_t p =  row%P;
	//	ghost_idx_t h =  ((row-p)/P)%H;
	////	ghost_idx_t w =  ((row-p-P*h)/(P*H))%W;
	////	ghost_idx_t l =  ((row-p-P*h-P*H*w)/(P*H*W))%L;

		ghost_idx_t l =  row                  /(W*H*P);
		ghost_idx_t w = (row-  l*W   *H   *P )/(  H*P);
		ghost_idx_t h = (row- (l*W+w)*H   *P )/(    P);
		ghost_idx_t p =  row-((l*W+w)*H+h)*P;
		
		double complex spinor[4][4];
		spinor[0][0]= 0.5;
		spinor[0][1]= 0.5;
		spinor[0][2]= 0.0;
		spinor[0][3]= 0.0;
		
		spinor[1][0]= 0.0;
		spinor[1][1]= 0.0;
		spinor[1][2]= 0.5;
		spinor[1][3]= 0.5;
		
		spinor[2][0]= 0.5;
		spinor[2][1]=-0.5;
		spinor[2][2]= 0.0;
		spinor[2][3]= 0.0;
		
		spinor[3][0]= 0.0;
		spinor[3][1]= 0.0;
		spinor[3][2]= 0.5;
		spinor[3][3]=-0.5;
		
		double complex * zval = val;
		
		ghost_idx_t i;
		//for(i=0;i<4*K_num;i++)  zval[i] = 0.;
		//for(i=0;i<  K_num;i++)  zval[p+i*4] =  cexp( I*(l*kx[i] + w*ky[i] + h*kz[i]) );
		
		if( h == 0 )   *zval = (spinor[0][p] + 1.*I*spinor[1][p])/sqrt( L*W );
		//if(!(H-1-h))   *zval = spinor[2][p] + I*spinor[3][p];
		else           *zval = 0.;
		


	return ;
}

*/
