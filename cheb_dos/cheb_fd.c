#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <ghost.h>
#include <ghost/instr.h>
#include <ghost/rand.h>
#include <essex-physics/matfuncs.h>
#include <essex-physics/cheb_toolbox.h>
#include <essex-physics/svqb.h>
#include <essex-physics/rayleigh_ritz.h>
#include "essexamples.h"

typedef struct args{
    int argc;
    char **argv;
} args;

void * cheb_fd(void *varg)
{
    args * arg = (args *)varg;
    int argc = arg->argc;
    char **argv = arg->argv;
    int m,i,rank;

    ghost_spmv_flags spmv_flags = GHOST_SPMV_DEFAULT;
    ghost_sparsemat_traits mtraits = GHOST_SPARSEMAT_TRAITS_INITIALIZER;

    ghost_sparsemat * mat;

    GHOST_INSTR_START("matrix_creation");
    essexamples_create_matrix(&mat,NULL,&mtraits);
    GHOST_INSTR_STOP("matrix_creation");

    ghost_rank(&rank,mat->context->mpicomm);

    cheb_fd_handle fd_handle;
    init_color_comm_handle( &(fd_handle.color_comm),  mat->context);
    fd_handle.eig_down   = -10.;
    fd_handle.eig_top    =  10.;
    fd_handle.lambda_min =  -1.;
    fd_handle.lambda_max =   1.;
    fd_handle.N_space  =  32;
    fd_handle.eps      = 1.e-7;
    char kernel[] = "Lo";
    fd_handle.kernel = kernel;
    double kernel_val = 2.;
    fd_handle.kernel_val = &kernel_val;
    fd_handle.ploy_factor = 8.;
    fd_handle.border_shift = 0.;
    fd_handle.max_Iter = 4;
    fd_handle.save_memory_mode = 0;
    fd_handle.final_cut_mode  = 0;
    fd_handle.file_prefix = NULL;
    fd_handle.globalSpmvmOptions = GHOST_SPMV_DEFAULT;
    
    
    essexamples_get_output_file( &(fd_handle.file_prefix) );
    essexamples_get_randvecnum( &(fd_handle.N_space) );

    essexamples_get_extremal_eig_range( &(fd_handle.eig_down), &(fd_handle.eig_top) );
    essexamples_get_target_eig_range( &(fd_handle.lambda_min), &(fd_handle.lambda_max) );

    essexamples_set_spmv_flags(&(fd_handle.globalSpmvmOptions));

    //mat->traits.opt_blockvec_width = 32;

    if( mat->traits.datatype == (GHOST_DT_DOUBLE|GHOST_DT_COMPLEX ) ){
        cheb_fd_window_z( mat, fd_handle);
    }else{
        cheb_fd_window_d( mat, fd_handle);
    }

    essexamples_print_info(mat,0);

    ghost_sparsemat_destroy(mat);
    return NULL;
}

int main(int argc, char* argv[])
{
    args arg;
    arg.argc = argc;
    arg.argv = argv;
    essexamples_process_options(argc,argv);

    ghost_init(argc,argv);
    //ghost_rand_seed(GHOST_RAND_SEED_TIME,123);

    ghost_task *t;
    ghost_task_create(&t,GHOST_TASK_FILL_ALL, 0, &cheb_fd, &arg, GHOST_TASK_DEFAULT, NULL, 0);
    ghost_task_enqueue(t);
    ghost_task_wait(t);
    ghost_task_destroy(t);
    ghost_finalize();

    return EXIT_SUCCESS;
}


