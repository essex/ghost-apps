#include <ghost.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "essexamples.h"

#define DP

#ifdef DP
    GHOST_REGISTER_DT_D(vecdt) // vectors have double values
    GHOST_REGISTER_DT_D(matdt) // matrix has double values
#define EPS 1e-32
#else
    GHOST_REGISTER_DT_S(vecdt) // vectors have float values
    GHOST_REGISTER_DT_S(matdt) // matrix has float values
#define EPS 1e-16
#endif

static void *mainTask(void *arg)
{
    int iteration =0, nIter = 1000;
    double start = 0.;
    matdt_t alpha = 0., alphaold = 0., lambda=0., lambdaold = 0., zero = 0., neg = -1., one = 1., tmp = 0.;
    matdt_t localdot[3];
    int myrank;

    ghost_sparsemat *A;
    ghost_densemat *x;
    ghost_densemat *r;
    ghost_densemat *b;
    ghost_densemat *p;
    ghost_densemat *v;

    essexamples_get_iterations(&nIter);

    ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;
    vtraits.datatype = vecdt;

    ghost_sparsemat_traits mtraits = GHOST_SPARSEMAT_TRAITS_INITIALIZER;
    essexamples_create_matrix(&A,NULL,&mtraits);

    ghost_spmv_flags spmvmOpt = GHOST_SPMV_DOT;
    ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
    spmvtraits.dot = localdot;
    spmvtraits.flags = GHOST_SPMV_DOT;

    ghost_rank(&myrank, A->context->mpicomm);

    essexamples_create_densemat(&x,&vtraits,ghost_context_max_map(A->context));
    essexamples_create_densemat(&b,&vtraits,ghost_context_max_map(A->context));
    essexamples_create_densemat(&r,&vtraits,ghost_context_max_map(A->context));
    essexamples_create_densemat(&v,&vtraits,ghost_context_max_map(A->context));
    essexamples_create_densemat(&p,&vtraits,ghost_context_max_map(A->context));

    ghost_densemat_init_val(x,&one);  // x = 1, start with defined value
    ghost_densemat_init_val(r,&zero); // r = 0
    ghost_densemat_init_val(b,&zero); // b = 0
    ghost_densemat_init_val(v,&zero); // v = 0

    ghost_spmv(r,A,x,spmvtraits); // r = A * x
    ghost_axpby(r,b,&one, &neg);  // r = -1*r + b
    ghost_dot(&alpha,r,r);        // alpha = ||r||

    ghost_densemat_init_densemat(p,r,0,0); // p = r

    printf("\n-------------------------------------\n");

    for(iteration = 0; iteration < nIter && alpha > EPS ; iteration++)
    {
        alphaold = alpha;
        lambdaold = lambda;

        localdot[0] = 0.;
        localdot[1] = 0.;
        localdot[2] = 0.;
        ghost_spmv(v,A,p,spmvtraits); // v = A * p

        lambda = localdot[1];

        lambda = alphaold / lambda; // ...

        ghost_instr_prefix_set("x_");
        ghost_axpy(x,p,&lambda); // x = x + lambda*p

        ghost_instr_prefix_set("r_");
        lambda *= -1.;
        ghost_axpy(r,v,&lambda); // r = r + lambda*v

        ghost_instr_prefix_set("");
        lambda *= -1.;

        ghost_dot(&alpha,r,r); // alpha = ||r||

        tmp = alpha/alphaold;

        ghost_axpby(p,r,&one,&tmp);

        if (myrank == 0)
            printf("\33[2K\ralpha[%6d] = %.4e",iteration+1, alpha);
        fflush(stdout);
    }
    
    printf("%s\n",alpha<EPS?" (converged)":" (max. iter)");
    
    // print norm of residual
    ghost_densemat_init_val(r,&zero);
    spmvtraits.flags = GHOST_SPMV_DEFAULT;
    ghost_spmv(r,A,x,spmvtraits);
    ghost_axpy(r,b,&neg);
    vecdt_t rnorm;
    ghost_dot(&rnorm,r,r);
    rnorm = sqrt(rnorm);
    printf("|Ax-b|      = %g\n",rnorm);
    printf("-------------------------------------\n");
    
    essexamples_print_info(A,0);

    ghost_densemat_destroy(v);
    ghost_densemat_destroy(r);
    ghost_densemat_destroy(x);
    ghost_densemat_destroy(p);
    ghost_densemat_destroy(b);
    ghost_sparsemat_destroy(A);

    return NULL;
}

int main(int argc, char* argv[])
{
    essexamples_process_options(argc,argv);

    ghost_init(argc,argv); 

    ghost_task *t;
    ghost_task_create(&t,GHOST_TASK_FILL_ALL,0,&mainTask,NULL,GHOST_TASK_DEFAULT, NULL, 0);

    ghost_task_enqueue(t);
    ghost_task_wait(t);
    ghost_task_destroy(t);
    ghost_finalize();

    return EXIT_SUCCESS;
}
