---
# Installation #
---

First of all, clone the git repository:

`git clone git@bitbucket.org:essex/ghost-apps.git && cd ghost-apps/`

It is preferrable to perform an out-of-source build, i.e., create a build directory first:

`mkdir build && cd build/`

To do a quick build with the default settings:

`cmake .. -DGHOST_DIR=<ghost-prefix>/lib/ghost -DESSEX-PHYSICS_DIR=<essex-physics-prefix>/lib/essex-physics`

You can toggle shared/static libs with `-DBUILD_SHARED_LIBS=ON/OFF` (default: static).
Once the Makefile is present you can type

`make`

For interactive specification of build options and variables, use ccmake to configure and generate a Makefile:

`ccmake ..`

