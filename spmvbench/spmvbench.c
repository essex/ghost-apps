#include <ghost.h>
#include <stdio.h>
#include "essexamples.h"

static void *mainTask(void *arg)
{
    ghost_spmv_opts spmvtraits = GHOST_SPMV_OPTS_INITIALIZER;
    int ferr, n, iteration, nIter = 0;
    int rank;
    const int printrank = 0;
    double start, end;

    ghost_sparsemat *mat;
    ghost_densemat *x, *y, *tmp;

    ghost_sparsemat_traits mtraits = GHOST_SPARSEMAT_TRAITS_INITIALIZER;
    ghost_densemat_traits vtraits = GHOST_DENSEMAT_TRAITS_INITIALIZER;

    essexamples_get_randvecnum(&vtraits.ncols);
    essexamples_set_spmv_flags(&spmvtraits.flags);
    essexamples_create_matrix(&mat,NULL,&mtraits);
    essexamples_get_iterations(&nIter);

    // we want to compute y = y+Ax
    spmvtraits.flags |= GHOST_SPMV_AXPY;

    ghost_rank(&rank, mat->context->mpicomm);

    vtraits.datatype = mat->traits.datatype;
    essexamples_create_densemat(&x,&vtraits,mat->context->col_map);
    essexamples_create_densemat(&y,&vtraits,mat->context->col_map);

    if(rank == printrank)
    {
        GHOST_WARNING_LOG("Currently setting column map vector to the result vector too. This will break if there is rectangular matrix or any unsymmetric permutation.");
    }
    ghost_densemat_init_rand(y); // y = rand
    ghost_densemat_init_rand(x); // x = rand

    ghost_barrier();
    ghost_timing_wcmilli(&start);
    for(iteration = 0; iteration < nIter; iteration++) {
        ghost_spmv(y,mat,x,spmvtraits);
        tmp = x;
        x = y;
        y = tmp;
        ghost_densemat_set_map(x,mat->context->col_map);
        ghost_densemat_set_map(y,mat->context->row_map);
    }
    ghost_barrier();
    ghost_timing_wcmilli(&end);

    essexamples_print_info(mat,printrank);

    if (rank == 0) {
        double maddflops = 2;
        if (mat->traits.datatype & GHOST_DT_COMPLEX) {
            maddflops = 8;
        }

        printf("%.2f Gflop/s\n",(mat->context->gnnz/1.e6*vtraits.ncols*nIter*maddflops)/(end-start));
    }

    ghost_densemat_destroy(x);
    ghost_densemat_destroy(y);
    ghost_sparsemat_destroy(mat);

    return NULL;
}

int main(int argc, char* argv[])
{
    essexamples_process_options(argc,argv);

    ghost_init(argc,argv); // has to be the first call

    ghost_task *t;
    ghost_task_create(&t,GHOST_TASK_FILL_ALL,0,&mainTask,NULL,GHOST_TASK_DEFAULT, NULL, 0);
    ghost_task_enqueue(t);
    ghost_task_wait(t);
    ghost_task_destroy(t);

    ghost_finalize();

    return EXIT_SUCCESS;
}
